package com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary;

import android.os.Handler;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Tigran on 11/21/2016.
 *
 * Class for downloading resources.
 * Prevents multiple downloads of the same resource simultaneously.
 * Allows cancellation of download. If one of the sources cancels download, it doesn't stop unless that source is the only one waiting.
 */
public class DownloadManager {

    private static DownloadManager instance;

    public static DownloadManager getInstance() {
        if (instance == null)
            instance = new DownloadManager();

        return instance;
    }

    private ExecutorService executor;
    private Map<String, List<DownloadCallback>> callbacks;
    private Map<String, Future> futures; // Future objects for canceling download tasks
    private Handler handler;

    private DownloadManager() {
        int coreNum = Runtime.getRuntime().availableProcessors();
        LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>();
        // Keep alive 60 seconds
        executor = new ThreadPoolExecutor(coreNum, coreNum, 60, TimeUnit.SECONDS, workQueue);

        callbacks = new HashMap<>();
        futures = new HashMap<>();
        handler = new Handler();
    }

    /**
     * Downloads the resource with given url asynchronously.
     * Decoder and callback should have the same generic type so callback will be called for a resource object decoded by the decoder.
     * @param url URL address of resource
     * @param decoder Decoder object for decoding raw downloaded data
     * @param callback
     * @param <T> Resource type
     * @return
     */
    public synchronized <T> CancellationToken downloadAsync(String url, DataDecoderBase<T> decoder, DownloadCallback<T> callback) {
        if (url == null)
            return null;

        List<DownloadCallback> urlCallbacks = callbacks.get(url);
        if (urlCallbacks == null) { // The first call for this url
            urlCallbacks = new ArrayList<>();
            callbacks.put(url, urlCallbacks);

            Future future = startDownload(url, decoder);
            // Save the Future object so it can be canceled.
            futures.put(url, future);
        } else {
            Log.i("DownloadManager", "Data for this url is already downloading, adding callback: " + url);
        }

        urlCallbacks.add(callback);

        return new CancellationToken(url, callback);
    }

    /**
     * Cancels download if source was the only one waiting.
     * Only removes callback otherwise.
     * @param token Cancellation token
     */
    public synchronized void cancel(CancellationToken token) {
        if (token == null)
            return;
        List<DownloadCallback> urlCallbacks = callbacks.get(token.url);
        if (urlCallbacks == null)
            return;

        urlCallbacks.remove(token.callback);
        if (!urlCallbacks.isEmpty())
            return;


        Future future = futures.get(token.url);
        // Do nothing if task was canceled after finishing, but handler didn't call executeCallbacksFor method yet.
        if (future.isDone())
            return;

        // Cancel the task
        future.cancel(true);
        // Remove callback list for this url
        callbacks.remove(token.url);
        // Remove Future object for this url
        futures.remove(token.url);

        Log.i("DownloadManager", "Download canceled: " + token.url);
    }

    private <T> Future startDownload(final String url, final DataDecoderBase<T> decoder) {
        return executor.submit(new Runnable() {

            @Override
            public void run() {
                HttpURLConnection urlConnection = null;
                try {
                    Log.i("DownloadManager", "Download started: " + url);

                    urlConnection = (HttpURLConnection) new URL(url).openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setRequestProperty("Connection", "close");

                    InputStream responseStream = urlConnection.getInputStream();

                    // Buffer size for chunks
                    int bufferSize = 8 * 1024; //8KB
                    byte[] buffer = new byte[bufferSize];
                    int readBytes;
                    // Output

                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    while ((readBytes = responseStream.read(buffer)) > 0) {
                        if (Thread.interrupted())
                            return;
                        outputStream.write(buffer, 0, readBytes);
                    }

                    final T result = decoder.decode(outputStream.toByteArray());
                    outputStream.close();

                    Log.i("DownloadManager", "Download finished: " + url);

                    // Execute callbacks for the decoded result
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            executeCallbacksFor(url, result);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null)
                        urlConnection.disconnect();
                }
            }
        });
    }

    private synchronized <T> void executeCallbacksFor(String url, T result) {
        List<DownloadCallback> urlCallbacks = callbacks.get(url);
        if (urlCallbacks == null)
            return;
        for (DownloadCallback callback : urlCallbacks)
            if (callback != null)
                callback.onDownloadCompleted(result); // Callback's generic will always match the decoder's because of downloadAsync method

        callbacks.remove(url);
        futures.remove(url);
    }

    /**
     * A class for canceling downloads.
     * Contains the url and the callback object.
     */
    public static final class CancellationToken {
        private String url;
        private DownloadCallback callback;

        private CancellationToken(String url, DownloadCallback callback) {
            this.url = url;
            this.callback = callback;
        }
    }

    interface DownloadCallback<T> {
        void onDownloadCompleted(T result);
    }
}