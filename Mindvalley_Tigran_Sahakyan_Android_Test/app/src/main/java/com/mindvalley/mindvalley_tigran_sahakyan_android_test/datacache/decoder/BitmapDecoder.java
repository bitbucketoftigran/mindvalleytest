package com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.decoder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DataDecoderBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.utils.ImageUtils;

/**
 * Created by Tigran on 11/22/2016.
 * An implementation of DataDecoderBase class for decoding Bitmaps.
 * Allows to give maximum sizes of bitmap.
 */

public class BitmapDecoder extends DataDecoderBase<Bitmap> {

    private int maxWidth;
    private int maxHeight;

    public BitmapDecoder() {
        this(0, 0);
    }

    public BitmapDecoder(int size) {
        this(size, size);
    }

    public BitmapDecoder(int maxWidth, int maxHeight) {
        if (maxHeight < 0)
            maxHeight = 0;
        if (maxWidth < 0)
            maxWidth = 0;

        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
    }

    /**
     * Scales the bitmap down to fit the specified max bounds.
     * @param data raw bytes
     * @return
     */
    @Override
    public Bitmap decode(byte[] data) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        if (maxWidth == 0 || maxHeight == 0)
            return BitmapFactory.decodeByteArray(data, 0, data.length);

        // Read only dimensions.
        BitmapFactory.decodeByteArray(data, 0, data.length, options);
        int originalWidth = options.outWidth;
        int originalHeight = options.outHeight;
        int tempWidth = originalWidth;
        int tempHeight = originalHeight;
        int scale = 1;

        // Calculate the minimum size of scaled bitmap which is not smaller than specified max bounds.
        while (tempWidth / 2 >= maxWidth && tempHeight / 2 >= maxHeight) {
            tempWidth /= 2;
            tempHeight /= 2;
            scale *= 2;
        }

        // Read bitmap in smaller sample size
        options = new BitmapFactory.Options();
        options.inSampleSize = scale;
        Bitmap smallerBitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);

        // Calculate fit bitmap size
        int[] fitBounds = ImageUtils.calculateFitSize(new int[]{smallerBitmap.getWidth(), smallerBitmap.getHeight()},new int[]{maxWidth, maxHeight});
        if (fitBounds[0] == smallerBitmap.getWidth() && fitBounds[1] == smallerBitmap.getHeight())
            return smallerBitmap;

        // Scale to fit max bounds.
        Bitmap fitBitmap = Bitmap.createScaledBitmap(smallerBitmap, fitBounds[0], fitBounds[1], true);
        smallerBitmap.recycle();

        Log.d("BitmapDecoder", "Original size: " + originalWidth + "x" + originalHeight + ", fit size: " + fitBounds[0] + "x" + fitBounds[1]);
        return fitBitmap;
    }

    @Override
    public int sizeOf(Bitmap data) {
        return data.getByteCount();
    }
}