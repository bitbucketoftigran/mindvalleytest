package com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary;

/**
 * Created by Tigran on 11/24/2016.
 *
 * An implementation of DataDecoderBase class for raw data downloads.
 */

public class RawDecoder extends DataDecoderBase<byte[]> {
    @Override
    public byte[] decode(byte[] data) {
        return data;
    }

    @Override
    public int sizeOf(byte[] data) {
        return data.length;
    }
}
