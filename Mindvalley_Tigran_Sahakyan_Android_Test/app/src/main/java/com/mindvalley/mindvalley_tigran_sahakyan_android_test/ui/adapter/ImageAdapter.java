package com.mindvalley.mindvalley_tigran_sahakyan_android_test.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.R;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DownloadManager;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.loader.BitmapLoader;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.loader.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tigran on 11/24/2016.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {

    private List<String> urls;
    private LayoutInflater inflater;
    private List<DownloadManager.CancellationToken> tokens;

    public ImageAdapter(Context context ) {
        urls = new ArrayList<>();
        tokens = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    public void setImageUrls(List<String> urls) {
        this.urls.clear();
        for (DownloadManager.CancellationToken token : tokens)
            ImageLoader.getInstance().cancel(token);
        tokens.clear();

        if (urls != null)
            addImageUrls(urls);
        notifyDataSetChanged();
    }

    public void addImageUrls(List<String> urls) {
        this.urls.addAll(urls);
    }

    @Override
    public int getItemCount() {
        return urls.size();
    }

    @Override
    public ImageAdapter.ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.grid_item, parent, false);
        return new ImageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ImageAdapter.ImageViewHolder holder, int position) {
        holder.image.setImageResource(R.drawable.empty_image);
        DownloadManager.CancellationToken token = BitmapLoader.getInstance().displayImage(urls.get(position), holder.image);
        if (token != null)
            tokens.add(token);
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        public ImageViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView;
        }
    }
}
