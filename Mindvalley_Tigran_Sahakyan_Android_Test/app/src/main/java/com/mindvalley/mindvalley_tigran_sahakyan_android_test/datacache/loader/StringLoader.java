package com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.loader;

import android.util.Log;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.CacheManagerBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DataDecoderBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DownloadManager;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.ResourceLoaderBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.decoder.StringDecoder;

/**
 * Created by Tigran on 11/23/2016.
 * A singleton implementation of ResourceLoaderBase class for String resources.
 */

public class StringLoader extends ResourceLoaderBase<String> {

    private static StringLoader instance;
    private static CacheManagerBase cacheManager;
    private static DownloadManager downloader;


    /**
     * Should be called before call of getInstance method.
     * @param cacheManager Cache manager implementation to use.
     * @param downloader DataDownloader to use
     */
    public static void init(CacheManagerBase cacheManager, DownloadManager downloader) {
        StringLoader.cacheManager = cacheManager;
        StringLoader.downloader = downloader;
    }

    public static StringLoader getInstance() {

        if (cacheManager == null) {
            Log.e("StringLoader", "Cache manager is not set. StringLoader.init() should be called first");
            return null;
        }

        if (downloader == null) {
            Log.e("StringLoader", "Download manager is not set. JsonObjectLoader.init() should be called first");
            return null;
        }

        if (instance == null)
            instance = new StringLoader(cacheManager, downloader);

        return instance;
    }

    private StringLoader(CacheManagerBase cacheManager, DownloadManager downloader) {
        super(cacheManager, downloader);
    }

    @Override
    protected DataDecoderBase<String> getDecoder() {
        return new StringDecoder();
    }
}
