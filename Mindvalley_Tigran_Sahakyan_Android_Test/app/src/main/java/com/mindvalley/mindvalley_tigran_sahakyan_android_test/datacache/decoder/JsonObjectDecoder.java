package com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.decoder;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DataDecoderBase;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by Tigran on 11/22/2016.
 * An implementation of DataDecoderBase class for decoding JSONObjects.
 */

public class JsonObjectDecoder extends DataDecoderBase<JSONObject> {
    @Override
    public JSONObject decode(byte[] data) {
        try {
            return new JSONObject(new String(data, "UTF-8"));
        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int sizeOf(JSONObject data) {
        return data.toString().length() * 2; // Just leave as is for now
    }
}
