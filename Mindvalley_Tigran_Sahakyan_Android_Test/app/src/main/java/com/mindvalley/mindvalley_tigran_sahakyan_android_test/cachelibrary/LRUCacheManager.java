package com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary;

import android.util.Log;
import android.util.LruCache;

/**
 * Created by Tigran on 11/22/2016.
 *
 * A singleton implementation of CacheManagerBase class.
 * Uses android.util.LruCache to store data.
 */

public class LRUCacheManager extends CacheManagerBase {

    private static LRUCacheManager instance;
    private static int maxSize;

    public static void init(int maxSize) {
        LRUCacheManager.maxSize = maxSize;
    }

    public static LRUCacheManager getInstance() {
        if (instance == null)
            instance = new LRUCacheManager();

        return instance;
    }

    private LruCache<String, CacheEntry> cache;

    private LRUCacheManager() {
        super();
        cache = new LruCache<String, CacheEntry>(maxSize) {
            @Override
            protected int sizeOf(String key, CacheEntry value) {
                return value.size;
            }
        };
    }

    @Override
    public <T> void save(String url, T data, int size) {
        cache.put(url, new CacheEntry<>(data, size));
        Log.d("LRUCacheManager", "New entry added with size: " + size + ". AvailableSpace: " + (cache.maxSize() - cache.size()) + "/" + cache.maxSize());
    }

    @Override
    public<T> T get(String url) {
        try {
            CacheEntry<T> entry = (CacheEntry<T>) cache.get(url);
            if (entry != null)
                return entry.data;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        return null;
    }
}