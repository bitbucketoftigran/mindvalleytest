package com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary;

/**
 * Created by Tigran on 11/24/2016.
 * A base class for resource loaders.
 * Tries to load a resource from cache. Downloads and stores in cache if failed.
 */

public abstract class ResourceLoaderBase<T> {

    private DownloadManager downloader;
    private CacheManagerBase cacheManager;
    private DataDecoderBase<T> decoder;

    protected ResourceLoaderBase(CacheManagerBase cacheManager, DownloadManager downloader) {
        this.downloader = downloader;
        this.cacheManager = cacheManager;
        decoder = getDecoder();
    }

    public DownloadManager.CancellationToken load(final String url, final ResourceLoadCallback<T> callback) {
        T cachedData = getFromCache(url);
        if (cachedData != null) {
            callback.onResourceLoaded(cachedData);
            return null;
        }

        return downloader.downloadAsync(url, decoder, new DownloadManager.DownloadCallback<T>() {
            @Override
            public void onDownloadCompleted(T result) {
                if (result != null)
                    saveToCache(url, result, decoder.sizeOf(result));
                callback.onResourceLoaded(result);
            }
        });
    }

    public void cancel(DownloadManager.CancellationToken token) {
        downloader.cancel(token);
    }

    private T getFromCache(String url) {
        return cacheManager.get(url);
    }

    private void saveToCache(String url, T result, int size) {
        cacheManager.save(url, result, size);
    }
    protected abstract DataDecoderBase<T> getDecoder();

    public interface ResourceLoadCallback<T> {
        void onResourceLoaded(T data);
    }
}