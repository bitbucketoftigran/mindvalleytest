package com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary;

/**
 * Created by Tigran on 11/23/2016.
 *
 * Base class for cache managers.
 * Derived classes must implement get and save methods.
 */
public abstract class CacheManagerBase {

    public abstract <T> T get(String url);
    public abstract <T> void save(String url, T data, int size);

    /**
     * Helper class for cache managers with memory limit
     * @param <T>
     */
    protected static class CacheEntry<T> {
        T data;
        int size;

        public CacheEntry(T data, int size) {
            this.data = data;
            this.size = size;
        }
    }
}