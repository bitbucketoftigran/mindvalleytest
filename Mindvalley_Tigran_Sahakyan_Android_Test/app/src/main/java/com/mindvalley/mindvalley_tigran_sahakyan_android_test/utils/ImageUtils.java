package com.mindvalley.mindvalley_tigran_sahakyan_android_test.utils;

/**
 * Created by Tigran on 11/22/2016.
 */

public class ImageUtils {

    /**
     * This method calculates bounds of a rectangle that fits maxBounds and has the same aspect ratio as the srcBounds
     * @param srcBounds
     * @param maxBounds
     * @return rectangle bounds
     */
    public static int[] calculateFitSize(int[] srcBounds, int[] maxBounds) {

        int srcWidth = srcBounds[0];
        int srcHeight = srcBounds[1];

        if (srcWidth <= maxBounds[0] && srcHeight <= maxBounds[1])
            return new int[]{srcWidth, srcHeight};

        double srcRatio = srcWidth / (double) srcHeight;
        double destRatio = maxBounds[0] / (double) maxBounds[1];

        int destWidth = maxBounds[0];
        int destHeight = maxBounds[1];

        if (srcRatio > destRatio) // Source is wider
            destHeight = srcHeight * destWidth / srcWidth;
        else if (srcRatio < destRatio)
            destWidth = srcWidth * destHeight / srcHeight;


        return new int[]{destWidth, destHeight};
    }
}
