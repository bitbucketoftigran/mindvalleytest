package com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.loader;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.CacheManagerBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DataDecoderBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DownloadManager;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.ResourceLoaderBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.decoder.BitmapDecoder;

/**
 * Created by Tigran on 11/23/2016.
 * A singleton implementation of ResourceLoaderBase class for Bitmap resources.
 */
public class BitmapLoader extends ResourceLoaderBase<Bitmap> {

    private static BitmapLoader instance;
    private static CacheManagerBase cacheManager;
    private static DownloadManager downloader;
    private static int maxBounds;

    /**
     * Should be called before call of getInstance method.
     * @param cacheManager Cache manager implementation to use.
     * @param downloader DataDownloader to use
     * @param maxBounds Maximum size of Bitmaps allowed. Downloaded images will be scaled to fit this size.
     */
    public static void init(CacheManagerBase cacheManager, DownloadManager downloader, int maxBounds) {
        BitmapLoader.cacheManager = cacheManager;
        BitmapLoader.downloader = downloader;
        BitmapLoader.maxBounds = maxBounds;
    }

    public static BitmapLoader getInstance() {

        if (cacheManager == null) {
            Log.e("StringLoader", "Cache manager is not set. BitmapLoader.init() should be called first");
            return null;
        }

        if (downloader == null) {
            Log.e("StringLoader", "Download manager is not set. JsonObjectLoader.init() should be called first");
            return null;
        }

        if (instance == null)
            instance = new BitmapLoader(cacheManager, downloader);

        return instance;
    }

    private BitmapLoader(CacheManagerBase cacheManager, DownloadManager downloader) {
        super(cacheManager, downloader);
    }

    /**
     * Loads image asynchronously and sets as source in given image view.
     * @param url URL of image resource.
     * @param imageView ImageView to display the image.
     * @return CancellationToken of the download.
     */
    public DownloadManager.CancellationToken displayImage(String url, final ImageView imageView) {
        return load(url, new ResourceLoadCallback<Bitmap>() {
            @Override
            public void onResourceLoaded(Bitmap bmp) {
                imageView.setImageBitmap(bmp);
            }
        });
    }

    @Override
    protected DataDecoderBase<Bitmap> getDecoder() {
        return new BitmapDecoder(maxBounds);
    }
}
