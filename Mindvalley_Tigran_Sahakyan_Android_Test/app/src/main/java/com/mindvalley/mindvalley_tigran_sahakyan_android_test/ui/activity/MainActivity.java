package com.mindvalley.mindvalley_tigran_sahakyan_android_test.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.Constants;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.R;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.ui.adapter.ImageAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    private View plusButton;
    private SwipeRefreshLayout swipeRefresh;
    private ImageAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        plusButton = findViewById(R.id.plus_button);
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        RecyclerView imagesGrid = (RecyclerView) findViewById(R.id.images_grid);

        RecyclerView.LayoutManager gridManager = new GridLayoutManager(this, 3);
        imagesGrid.setLayoutManager(gridManager);

        adapter = new ImageAdapter(this);
        imagesGrid.setAdapter(adapter);

        // Hide/show X button when scrolled down/up
        imagesGrid.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy < 0 && plusButton.getVisibility() != View.VISIBLE) {
                    plusButton.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, R.anim.show_scale));
                    plusButton.setVisibility(View.VISIBLE);
                } else if (dy > 0 && plusButton.getVisibility() == View.VISIBLE){
                    plusButton.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, R.anim.hide_scale));
                    plusButton.setVisibility(View.INVISIBLE);
                }
            }
        });

        swipeRefresh.setColorSchemeResources(R.color.colorPrimary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchImages();
            }
        });

        fetchImages();
    }

    private void fetchImages() {
        List<String> imageUrls = new ArrayList<>();
        for (int i = 0; i < 19; i++)
            imageUrls.add(String.format(Constants.IMAGES_URL, i));

        adapter.setImageUrls(imageUrls);
        swipeRefresh.setRefreshing(false);
    }

    // Clear all images and stop downloads
    public void clickPlus(View v) {
        adapter.setImageUrls(null);
    }
}
