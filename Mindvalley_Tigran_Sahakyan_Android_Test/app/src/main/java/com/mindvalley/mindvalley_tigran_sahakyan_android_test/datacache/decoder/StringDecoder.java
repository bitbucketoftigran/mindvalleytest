package com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.decoder;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DataDecoderBase;

import java.io.UnsupportedEncodingException;

/**
 * Created by Tigran on 11/22/2016.
 * An implementation of DataDecoderBase class for decoding Strings.
 */

public class StringDecoder extends DataDecoderBase<String> {
    @Override
    public String decode(byte[] data) {
        try {
            return new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int sizeOf(String data) {
        return data.length() + 2;
    }
}
