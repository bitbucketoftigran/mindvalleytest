package com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.decoder;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DataDecoderBase;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;

/**
 * Created by Tigran on 11/22/2016.
 * An implementation of DataDecoderBase class for decoding JSONArrays.
 */

public class JsonArrayDecoder extends DataDecoderBase<JSONArray> {
    @Override
    public JSONArray decode(byte[] data) {
        try {
            return new JSONArray(new String(data, "UTF-8"));
        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int sizeOf(JSONArray data) {
        return data.toString().length() * 3; // Just leave as is for now
    }
}
