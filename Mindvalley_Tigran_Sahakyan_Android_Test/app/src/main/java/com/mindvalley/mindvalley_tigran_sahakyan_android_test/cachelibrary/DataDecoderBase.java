package com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary;

/**
 * Created by Tigran on 11/22/2016.
 *
 * Base class for data decoder classes.
 * Instance of implementation a derived class should be passed to DownloadManager.downloadAsync method.
 * decode method will be called by DownloadManager when raw data bytes are received.
 */
public abstract class DataDecoderBase<T> {

    /**
     * Decode raw data bytes into data object
     * @param data raw bytes
     * @return decoded object
     */
    public abstract T decode(byte[] data);

    /**
     * Implementation of this method should return size of data object in memory.
     * @param data data object
     * @return size in memory
     */
    public abstract int sizeOf(T data);
}