package com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.loader;

import android.util.Log;
import android.widget.ImageView;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.CacheManagerBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DataDecoderBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DownloadManager;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.RawDecoder;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.ResourceLoaderBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.decoder.BitmapDecoder;

/**
 * Created by Tigran on 11/24/2016.
 * A singleton implementation of ResourceLoaderBase class for raw image resources.
 */

public class ImageLoader extends ResourceLoaderBase<byte[]> {

    private static ImageLoader instance;
    private static CacheManagerBase cacheManager;
    private static DownloadManager downloader;
    private static int maxBounds;

    /**
     * Should be called before call of getInstance method.
     * @param cacheManager Cache manager implementation to use.
     * @param downloader DataDownloader to use
     * @param maxBounds Maximum size of Bitmaps allowed. Downloaded images will be scaled to fit this size.
     */
    public static void init(CacheManagerBase cacheManager, DownloadManager downloader, int maxBounds) {
        ImageLoader.cacheManager = cacheManager;
        ImageLoader.downloader = downloader;
        ImageLoader.maxBounds = maxBounds;
    }

    public static ImageLoader getInstance() {

        if (cacheManager == null) {
            Log.e("StringLoader", "Cache manager is not set. BitmapLoader.init() should be called first");
            return null;
        }

        if (downloader == null) {
            Log.e("StringLoader", "Download manager is not set. JsonObjectLoader.init() should be called first");
            return null;
        }

        if (instance == null)
            instance = new ImageLoader(cacheManager, downloader);

        return instance;
    }

    private ImageLoader(CacheManagerBase cacheManager, DownloadManager downloader) {
        super(cacheManager, downloader);
    }

    public DownloadManager.CancellationToken displayImage(String url, final ImageView imageView) {
        return load(url, new ResourceLoadCallback<byte[]>() {
            @Override
            public void onResourceLoaded(byte[] imageSrc) {
                imageView.setImageBitmap(new BitmapDecoder(maxBounds).decode(imageSrc));
            }
        });
    }

    @Override
    protected DataDecoderBase<byte[]> getDecoder() {
        return new RawDecoder();
    }
}
