package com.mindvalley.mindvalley_tigran_sahakyan_android_test;

import android.app.Application;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DownloadManager;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.LRUCacheManager;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.loader.BitmapLoader;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.loader.ImageLoader;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.loader.JsonObjectLoader;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.loader.StringLoader;

/**
 * Created by Tigran on 11/23/2016.
 */

public class MindValleyTestApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DownloadManager downloader = DownloadManager.getInstance();
        LRUCacheManager.init(10 * 1024 * 1024); // 10M
        ImageLoader.init(LRUCacheManager.getInstance(), downloader, 1200);
        // Maximum size of images for bitmap loader should be as small as possible because bitmap objects are stored in memory.
        BitmapLoader.init(LRUCacheManager.getInstance(), downloader, 500);
        StringLoader.init(LRUCacheManager.getInstance(), downloader);
        JsonObjectLoader.init(LRUCacheManager.getInstance(), downloader);
    }
}