package com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.loader;

import android.util.Log;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.CacheManagerBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DataDecoderBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.DownloadManager;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.cachelibrary.ResourceLoaderBase;
import com.mindvalley.mindvalley_tigran_sahakyan_android_test.datacache.decoder.JsonObjectDecoder;

import org.json.JSONObject;

/**
 * Created by Tigran on 11/23/2016.
 * A singleton implementation of ResourceLoaderBase class for JSONObject resources.
 */

public class JsonObjectLoader extends ResourceLoaderBase<JSONObject> {

    private static JsonObjectLoader instance;
    private static CacheManagerBase cacheManager;
    private static DownloadManager downloader;


    /**
     * Should be called before call of getInstance method.
     * @param cacheManager Cache manager implementation to use.
     * @param downloader DataDownloader to use
     */
    public static void init(CacheManagerBase cacheManager, DownloadManager downloader) {
        JsonObjectLoader.cacheManager = cacheManager;
        JsonObjectLoader.downloader = downloader;
    }

    public static JsonObjectLoader getInstance() {

        if (cacheManager == null) {
            Log.e("StringLoader", "Cache manager is not set. JsonObjectLoader.init() should be called first");
            return null;
        }

        if (downloader == null) {
            Log.e("StringLoader", "Download manager is not set. JsonObjectLoader.init() should be called first");
            return null;
        }

        if (instance == null)
            instance = new JsonObjectLoader(cacheManager, downloader);

        return instance;
    }

    private JsonObjectLoader(CacheManagerBase cacheManager, DownloadManager downloader) {
        super(cacheManager, downloader);
    }

    @Override
    protected DataDecoderBase<JSONObject> getDecoder() {
        return new JsonObjectDecoder();
    }
}
