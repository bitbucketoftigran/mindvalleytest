package com.mindvalley.mindvalley_tigran_sahakyan_android_test;

import com.mindvalley.mindvalley_tigran_sahakyan_android_test.utils.ImageUtils;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests for utility classes.
 */
public class UtilTests {
    @Test
    public void fit_calculation_isCorrect() throws Exception {
        int[] srcBounds = new int[2];
        int[] maxBounds = new int[2];
        int[] expectedResult = new int[2];

        srcBounds[0] = 2000;
        srcBounds[1] = 1000;
        maxBounds[0] = 1500;
        maxBounds[1] = 1500;
        expectedResult[0] = 1500;
        expectedResult[1] = 750;
        assertArrayEquals(expectedResult, ImageUtils.calculateFitSize(srcBounds, maxBounds));

        maxBounds[0] = 1000;
        maxBounds[1] = 1000;
        expectedResult[0] = 1000;
        expectedResult[1] = 500;
        assertArrayEquals(expectedResult, ImageUtils.calculateFitSize(srcBounds, maxBounds));

        maxBounds[0] = 3000;
        maxBounds[1] = 3000;
        expectedResult[0] = 2000;
        expectedResult[1] = 1000;
        assertArrayEquals(expectedResult, ImageUtils.calculateFitSize(srcBounds, maxBounds));

        maxBounds[0] = 1000;
        maxBounds[1] = 400;
        expectedResult[0] = 800;
        expectedResult[1] = 400;
        assertArrayEquals(expectedResult, ImageUtils.calculateFitSize(srcBounds, maxBounds));

        maxBounds[0] = 400;
        maxBounds[1] = 1000;
        expectedResult[0] = 400;
        expectedResult[1] = 200;
        assertArrayEquals(expectedResult, ImageUtils.calculateFitSize(srcBounds, maxBounds));

        srcBounds[0] = 1000;
        srcBounds[1] = 500;
        expectedResult[0] = 400;
        expectedResult[1] = 200;
        assertArrayEquals(expectedResult, ImageUtils.calculateFitSize(srcBounds, maxBounds));

        maxBounds[0] = 1000;
        maxBounds[1] = 1000;
        expectedResult[0] = 1000;
        expectedResult[1] = 500;
        assertArrayEquals(expectedResult, ImageUtils.calculateFitSize(srcBounds, maxBounds));

        maxBounds[0] = 2000;
        maxBounds[1] = 2000;
        assertArrayEquals(expectedResult, ImageUtils.calculateFitSize(srcBounds, maxBounds));

        maxBounds[0] = 200;
        maxBounds[1] = 2000;
        expectedResult[0] = 200;
        expectedResult[1] = 100;
        assertArrayEquals(expectedResult, ImageUtils.calculateFitSize(srcBounds, maxBounds));
    }
}